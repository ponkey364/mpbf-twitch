module gitlab.com/ponkey364/mpbf-twitch

go 1.16

require (
	github.com/gempir/go-twitch-irc/v2 v2.6.0
	gitlab.com/ponkey364/mpbf v0.0.0-20210912112701-123012b2fc2d
)
