package mpbf_twitch

import (
	"context"

	"github.com/gempir/go-twitch-irc/v2"
	"gitlab.com/ponkey364/mpbf"
)

const PlatformName = "twitch"

type TwitchPlatform struct {
	session *twitch.Client
}

func (t *TwitchPlatform) Open(mc *mpbf.BotCommander, onMessage mpbf.CommandHandler) error {
	t.session.OnPrivateMessage(func(message twitch.PrivateMessage) {
		msg := &mpbf.Message{
			Content: message.Message,
			Author: &mpbf.User{
				ID:   message.User.ID,
				Name: message.User.Name,
			},
			Realm: &mpbf.Realm{
				ID: message.Channel,
			},
		}

		ctx := context.WithValue(context.Background(), "twitch.message", message)
		ctx = context.WithValue(ctx, "platform", PlatformName)

		onMessage(&mpbf.Request{Message: msg, Context: ctx}, nil)
	})

	err := t.session.Connect()
	if err != nil {
		return err
	}

	return nil
}

func (t *TwitchPlatform) Close() error {
	return t.session.Disconnect()
}

func (t *TwitchPlatform) GetPlatformType() string {
	return PlatformName
}

func (t *TwitchPlatform) JoinRealm(realmID string) error {
	t.session.Join(realmID)
	return nil
}

func (t *TwitchPlatform) LeaveRealm(realmID string) error {
	t.session.Depart(realmID)
	return nil
}

func (t *TwitchPlatform) GetSession() interface{} {
	return t.session
}

func (t *TwitchPlatform) GetArgParser() func(rawArgs []string, command *mpbf.Command) ([]interface{}, error) {
	return mpbf.DefaultArgParser
}

func New(username string, token string) (mpbf.Platform, error) {
	session := twitch.NewClient(username, token)

	return &TwitchPlatform{
		session: session,
	}, nil
}
